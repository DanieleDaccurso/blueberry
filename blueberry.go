package blueberry

import (
	"gitlab.com/danieledaccurso/blueberry/app"
)


func New() *app.App {
	return app.New()
}

func CreateDefault() *app.App {
	a := New()
	a.EnableCORS()
	a.EnableJSONResponse()
	a.EnableParams()
	return a
}
