package app

type ApplicationStartupEvent interface {
	Exec()
}

type ApplicationStartupEventContext struct {
	Application *App
	Config *AppConfig
}
